[Bludit Themes](https://themes.bludit.com)
================================
---> This repository is no longer maintained. <---

These themes are only compatible with Bludit v1.x., NOT for Bludit v2.x.

The new repository for Bludit themes is this
- https://github.com/bludit/themes
